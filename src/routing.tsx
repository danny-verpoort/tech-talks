import React from 'react';
import * as presentations from './presentations/index'
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import Home from "./home";


function Routing() {

    const presentationClasses = Object.values(presentations);

    return (
        <div className="App">
            <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet" />
            <Router>
                <Switch>
                    {presentationClasses.map((presentation, index) =>
                        <Route key={presentation.uri} path={"/tech-talks/" + presentation.uri}>
                            {React.createElement(presentation)}
                        </Route>
                    )}
                    <Route path="/">
                        <Home presentations={presentationClasses}/>
                    </Route>
                </Switch>
            </Router>
        </div>
    );
}

export default Routing;
