import React from "react";
import {Presentation} from "../Presentation";
import {H2, Header, HighlightPlugin, Li, RevealJS, Slide, Ul} from "@gregcello/revealjs-react";
import logo from "../../resources/images/logo.svg";
import danny from "../../resources/images/danny.png";
import bgImage from "../../resources/images/background.jpg";
import cleanCode from "./img/clean-code.jpeg";
import cleanCoder from "./img/clean-coder.jpeg";
import cleanAgile from "./img/clean-agile.jpg";
import clean from "./img/clean.png";
import cleanArchitecture from "./img/clean-architecture.jpeg";

require("highlight.js/scss/darcula.scss")
const style = ".reveal pre code { max-height: 100%;}"

class CleanAndSOLID extends Presentation({
    title: "Clean Architecture & SOLID principles",
    date: new Date(2021, 6, 19, 15, 30, 0),
    description: "Het bouwen van robuuste software door middel van clean architecture en SOLID principes",
    uri: "clean-and-bem"
}) {
    render = () => (
        <div style={{width: '100%', height: '100%'}}>
            <style>
                {style}
            </style>
            <Header className={"slide__top-bar"}>
                <div className={"slide__top-bar-container"}>
                    <div style={{textAlign: 'left'}}>
                        <img src={logo} alt="BouWatch" style={{height: '1.5em'}}/>
                    </div>
                    <div style={{textAlign: 'right'}}><img src={danny} alt="BouWatch" style={{height: '3em'}}/></div>
                </div>
            </Header>
            <RevealJS width={window.innerWidth} height={window.innerHeight} slideNumber={"c/t"} fragmentInURL={true}
                      hash={true} plugins={[HighlightPlugin]}>
                <Slide backgroundImage={bgImage}>
                    <div className="slide--title theme--bouwatch">
                        <div className="slide__header-container">
                            <h1 className="slide__header">{CleanAndSOLID.title}</h1>
                        </div>
                    </div>
                </Slide>
                <Slide>
                    <H2>Agenda</H2>
                    <Ul>
                        <Li>Clean Architecture</Li>
                        <Ul>
                            <li>Practical example<br/><br/></li>
                        </Ul>
                        <Li>SOLID principe</Li>
                        <Ul>
                            <li>Each underlying principle</li>
                            <li>Practical examples</li>
                            <br/>
                        </Ul>
                        <Li>Questions</Li>
                        <Li>Further reading & credits</Li>
                    </Ul>
                </Slide>
                <Slide>
                    <Slide>
                        <H2>Clean Architecture</H2>
                        <p>&nbsp;</p>
                        <p className={'fragment'}>First coined by <a href="http://cleancoder.com/"
                                                                     target={'_blank'} rel={'noreferrer'}> Robert C. Martin (Uncle Bob)</a>
                        </p>
                        <p>&nbsp;</p>
                        <div className={'fragment'}>
                            <img style={{width: '15%', marginRight: '2%'}} src={cleanCode} alt="Clean Code"/>
                            <img style={{width: '15%', marginRight: '2%'}} src={cleanCoder} alt="Clean Coder"/>
                            <img style={{width: '15%', marginRight: '2%'}} src={cleanAgile} alt="Clean Agile"/>
                            <img style={{width: '15%', marginRight: '2%'}} src={cleanArchitecture} alt="Clean Architecture"/>
                        </div>
                    </Slide>
                    <Slide>
                        <H2>Clean Architecture</H2>
                        <p>&nbsp;</p>
                        <p>A marriage of several architectures:</p>
                        <p>&nbsp;</p>
                        <Ul>
                            <Li><a href="https://alistair.cockburn.us/hexagonal-architecture/">Hexagonal
                                Architecture</a> by <a href="https://alistair.cockburn.us">A. Cockburn</a></Li>
                            <Li><a href="https://jeffreypalermo.com/2008/07/the-onion-architecture-part-1/">The Onion
                                Architecture</a> by <a href="https://jeffreypalermo.com">J. Palermo</a></Li>
                            <Li><a href="https://blog.cleancoder.com/uncle-bob/2011/09/30/Screaming-Architecture.html">Screaming
                                Architecture</a> by <a href="http://cleancoder.com/">R.C. Martin</a></Li>
                            <Li><a
                                href="https://www.amazon.com/Lean-Architecture-Agile-Software-Development/dp/0470684208/">Data,
                                context and interaction</a> by <a
                                href="https://sites.google.com/a/gertrudandcope.com/www/jimcoplien">J.
                                Coplien</a> and <a
                                href="https://folk.universitetetioslo.no/trygver/">T. Reenskaug</a></Li>
                            <Li><a
                                href="https://www.amazon.com/Object-Oriented-Software-Engineering-Approach/dp/0201544350">Entity-Control-Boundary</a> by <a
                                href="https://www.ivarjacobson.com/">Ivar Jacobson</a></Li>
                        </Ul>
                    </Slide>
                    <Slide>
                        <H2>Clean Architecture</H2>
                        <img src={clean} alt="Clean Architecture"/>
                    </Slide>
                    <Slide>
                        <H2>Entities</H2>
                    </Slide>
                    <Slide>
                        <H2>Entities</H2>
                    </Slide>
                    <Slide>
                        <H2>Use Cases</H2>
                    </Slide>
                    <Slide>
                        <H2>Use Cases</H2>
                    </Slide>
                    <Slide>
                        <H2>Interface adapters</H2>
                    </Slide>
                    <Slide>
                        <H2>Interface adapters</H2>
                    </Slide>
                    <Slide>
                        <H2>Frameworks & drivers</H2>
                    </Slide>
                    <Slide>
                        <H2>Frameworks & drivers</H2>
                    </Slide>
                </Slide>
                <Slide>
                    <Slide>
                        <H2>SOLID</H2>
                        <Ul>
                            <Li>Single responsibility principle</Li>
                            <Li>Open/closed principle</Li>
                            <Li>Liskov substitution principle</Li>
                            <Li>Interface segregation principle</Li>
                            <Li>Dependency inversion principle</Li>
                        </Ul>
                    </Slide>
                    <Slide>
                        <H2>Single responsibility principle</H2>
                    </Slide>
                    <Slide>
                        <H2>Open/closed principle</H2>
                    </Slide>
                    <Slide>
                        <H2>Liskov substitution principle</H2>
                    </Slide>
                    <Slide>
                        <H2>Interface segregation principle</H2>
                    </Slide>
                    <Slide>
                        <H2>Dependency inversion principle</H2>
                    </Slide>
                </Slide>
                <Slide>
                    <H2>Vragen?</H2>
                </Slide>
            </RevealJS>
        </div>
    );
}

export default CleanAndSOLID;
