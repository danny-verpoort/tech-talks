import React from "react";

/* eslint-disable */
abstract class _Presentation<P,S> extends React.Component{
    static title: string
    static description: string
    static date: Date
    static uri: string
}

export type PresentationType = typeof _Presentation

export function Presentation<P,S>(mandatory: {
    title: string
    description: string
    date: Date
    uri: string
}) {
    return class extends _Presentation<P, S> {
        static title = mandatory.title
        static description = mandatory.description
        static date = mandatory.date
        static uri = mandatory.uri
    }
}