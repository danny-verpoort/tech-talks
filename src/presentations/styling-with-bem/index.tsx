import React from "react";
import "../../themes/bouwatch/index.scss"
import {Presentation} from "../Presentation";
import {Code, H2, H3, Header, HighlightPlugin, Li, RevealJS, Slide, Ul} from "@gregcello/revealjs-react";
import logo from "../../resources/images/logo.svg";
import danny from "../../resources/images/danny.png";
import bgImage from "../../resources/images/background.jpg";
import pages from "../../resources/images/pages.jpeg";
import bootstrapPages from "../../resources/images/bootstrappedSites.png";
import gh from "../../resources/images/github.png"
import npm from "../../resources/images/nnpm.png"
import twitter from "../../resources/images/twitter.png"
import rc from "../../resources/images/rcbrand.png"
import pt from "../../resources/images/periodictable.png"
import ad from "../../resources/images/atomicdesign.png"
import atom from "../../resources/images/atom.png"
import molecule from "../../resources/images/molecule.png"
import org from "../../resources/images/organisms.png"
import page from "../../resources/images/page.png"
import template from "../../resources/images/template.png"
import overview from "../../resources/images/atomicdesignoverview.png"
require("highlight.js/scss/darcula.scss")
const style = ".reveal pre code { max-height: 100%;}"

class StylingWithBEM extends Presentation ({
    title: "Styling with atomic design and BEM",
    date: new Date(2021,2,1, 15,30,0),
    description: "Een inzicht in het styling van een web application door middel van de Block-Element-Modifier structuur.",
    uri: "styling-with-bem"
}) {
    render = () => (
        <div style={{width: '100%', height: '100%'}}>
            <style>
                {style}
            </style>
            <Header className={"slide__top-bar"}>
                <div className={"slide__top-bar-container"}>
                    <div style={{textAlign: 'left'}}>
                        <img src={logo} alt="BouWatch" style={{height: '1.5em'}}/>
                    </div>
                    <div style={{textAlign: 'right'}}><img src={danny} alt="BouWatch" style={{height: '3em'}}/></div>
                </div>
            </Header>
            <RevealJS width={window.innerWidth} height={window.innerHeight} slideNumber={"c/t"} fragmentInURL={true}
                      hash={true} plugins={[HighlightPlugin]}>
                <Slide backgroundImage={bgImage}>
                    <div className="slide--title theme--bouwatch">
                        <div className="slide__header-container">
                            <h1 className="slide__header">{StylingWithBEM.title}</h1>
                        </div>
                    </div>
                </Slide>
                <Slide>
                    <H2>Agenda</H2>
                    <Ul>
                        <Li>Atomic Design</Li>
                        <Li>BEM</Li>
                        <Li>Project structure</Li>
                        <Li>Class Structure</Li>
                        <Li>Demo</Li>
                        <Li>Q&A / Discussion</Li>
                    </Ul>
                </Slide>
                <Slide backgroundImage={pages}>
                </Slide>
                <Slide>
                    <H2>How long will it take...</H2>
                    <H3>To build a one page website</H3>
                </Slide>
                <Slide backgroundIframe={'https://dannyverpoort.nl'}>
                </Slide>
                <Slide backgroundIframe={'http://www.r2d3.us/visual-intro-to-machine-learning-part-1/'}>
                </Slide>
                <Slide>
                    <H2>Modularity</H2>
                    <p style={{margin:'1em 0'}}>Modularity has found its way into software:</p>
                    <Ul className={'fragment'}>
                        <Li>Object Oriented</Li>
                        <Li>Single Responsibility Principle</Li>
                        <Li>Domain Driven Development</Li>
                        <Li>Microservices</Li>
                        <Li>...</Li>
                    </Ul>
                    <p className={'fragment'} style={{marginTop:'1em'}}>It's just as important in frontend!</p>
                </Slide>
                <Slide backgroundImage={bootstrapPages}>
                </Slide>
                <Slide>
                    <H2>Unique brand identity?</H2>
                    <p>Lots of sites use Bootstrap:</p>
                    <Ul style={{listStyle: 'none', margin: 0, padding: 0}}>
                        <Li><img src={gh} height={'50px'} style={{margin: '0 15px 0 0'}} alt="github"/><span>149,000 stars</span></Li>
                        <Li><img src={gh} height={'50px'} style={{margin: '0 15px 0 0'}} alt="github"/><span>72,000 forks</span></Li>
                        <Li><img src={npm} height={'50px'} style={{margin: '0 15px 0 0'}} alt="github"/><span>3,180,731 <b>weekly</b></span></Li>
                        <Li><img src={twitter} height={'50px'} style={{margin: '0 15px 0 0'}} alt="github"/><span>557,600 followers</span></Li>
                    </Ul>
                </Slide>
                <Slide>
                    <H2>Brand identity</H2>
                    <img src={rc} style={{width: '40%'}} alt=""/>
                </Slide>
                <Slide backgroundImage={pt} backgroundSize={'100%'} backgroundColor={'#787878'}>
                </Slide>
                <Slide>
                    <H2>Chemistry analogy</H2>
                    <Ul>
                        <Li><b>Atoms</b> - The basic building blocks of all things</Li>
                        <Li><b>Molecules</b> - Groups of atoms make for example H<sub>2</sub>O</Li>
                        <Li><b>Organisms</b> - groups of molecules forming cells, bacerias, humans, ...</Li>
                    </Ul>
                </Slide>
                <Slide>
                    <H2>Atomic design</H2>
                    <p>The chemistry analogies form the basics of atomic design:</p>
                    <img src={ad} style={{width: '70%'}} alt={'atomic design'}/>
                </Slide>
                <Slide>
                    <H2>Atoms</H2>
                    <p>The smallest unit on the page, can't be broken down any further</p>
                    <img src={atom} style={{width: '40%'}} alt={'atom'}/>
                </Slide>
                <Slide>
                    <H2>Molecules</H2>
                    <p>Atoms combined to form a meaningful unit</p>
                    <img src={molecule} alt={'molecules'}/>
                </Slide>
                <Slide>
                    <H2>Organisms</H2>
                    <p>More complex clusters of molecules. Don't have to have a single responsibility</p>
                    <img src={org} alt={'organisms'}/>
                </Slide>
                <Slide>
                    <H2>Template</H2>
                    <p>An abstract version of the page with no meaningful content</p>
                    <img src={template} width={'30%'} alt={'templates'}/>
                </Slide>
                <Slide>
                    <H2>Page</H2>
                    <p>An actual meaningful page with content</p>
                    <img src={page} width={'30%'} alt={'pages'}/>
                </Slide>
                <Slide backgroundImage={overview} backgroundSize={'80%'} backgroundColor={'#f4f2db'}>
                </Slide>
                <Slide>
                    <H2>Can we get into some code plz?!</H2>
                    <Code children={{
                        code: 'base/\n' +
                        '├  _normalize.scss\n' +
                        '└  _typography.scss\n\n' +
                        'atom/\n' +
                        '└  _a-buttons.scss\n\n' +
                        'molecule/\n' +
                        '├  _m-site-menu.scss\n' +
                        '└  _m-card.scss\n\n' +
                        'organism/\n' +
                        '├  _o-teaser-list.scss\n' +
                        '└  _o-card-list.scss\n\n' +
                        'template/\n' +
                        '├  _t-section.scss\n' +
                        '└  _t-article.scss\n\n' +
                        'page/\n' +
                        '├  _p-blog.scss\n' +
                        '└  _p-home.scss\n\n' +
                        'style.scss'
                    }} language={'yaml'} lineNumbers={true} style={{width:'fit-content', backgroundColor: '#2b2b2b', maxHeight: '100%', padding: '5px 20px'}}/>
                </Slide>
                <Slide>
                    <H2>The BEM principe</H2>
                    <p>We split UI elements up in blocks, elements and modifiers.</p>
                    <Code children={{
                        code: '<div class="o-page-header">\n' +
                            '    <ul class="m-site-menu">\n' +
                            '        <li class="m-site-menu__link m-site-menu__link--active">Nav 2</li>\n' +
                            '        <li class="m-site-menu__link">Nav 1</li>\n' +
                            '    </ul>\n' +
                            '    <button class="a-button a-button--primary js-logout-button">logout</button>\n' +
                            '</div>'
                    }} language={'html'} lineNumbers={true} style={{width:'fit-content', backgroundColor: '#2b2b2b', maxHeight: '100%', padding: '5px 20px'}}/>
                </Slide>
                <Slide>
                    <H2>Demo time</H2>
                </Slide>
                <Slide>
                    <H2>Vragen?</H2>
                </Slide>
            </RevealJS>
        </div>
    );
}

export default StylingWithBEM;
