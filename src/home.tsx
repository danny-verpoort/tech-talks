import React from "react";
import { Link } from 'react-router-dom';

class Home extends React.Component<any> {
    render() {
        return <div className="homepage">
            <h1>Presentations</h1>
            <ul>
                {this.props.presentations.map((presentation: any, index: number) =>
                    <li key={index}>
                        <Link to={presentation.uri}>{presentation.title}</Link>
                    </li>
                )}
            </ul>
        </div>
    };
}

export default Home;